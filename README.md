A very _very VERY_ Work In Progress Subsurface Scattering shader for Unity BIRP, built with [ORELS Shaders](shaders.orels.sh) and based heavily on:
- https://therealmjp.github.io/posts/sg-series-part-3-diffuse-lighting-from-an-sg-light-source/
- https://therealmjp.github.io/posts/sss-sg/
- https://www.youtube.com/watch?v=ipge1K51JJs

![Video Demostration](https://fish.hareware.xyz/files/59024e84-d9d8-4fc5-ae11-11dfa257e4e6)
